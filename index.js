const express = require('express');
const port = 3000;
const cors = require('cors');
const morgan = require('morgan');
const app3k = express();


app3k.use(morgan('dev'));
app3k.use(cors());
app3k.use(express.json());
app3k.use(express.urlencoded({ extended: false }));
app3k.use('/static', express.static('upload'));

app3k.get("/", (req,res)=>{
    res.json({"message":"ok"});
    console.log("this is a test");
});

app3k.listen(port,()=>{
    console.log("app3k listening on PORT:3000");
})

app3k.listen(4000,()=>{
    console.log("app3k listening on PORT:4000");
})